import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

public abstract class Bot {
	protected final ArrayList<Integer[]> patterns;
	protected String mark;									// String that bot uses to check which mark is its own
	protected String opponentMark;					// String that bot uses to check which mark is opponent's
	protected Boolean checkForWinningMove;	// Boolean if bot should check for logistic winning moves
	protected Instances insts;							// Instances to use to build the tree
	protected String instsMark;							// String that indicates the optimized mark for instances
	protected String targetClassValue;			// Target class value in instances

	public Bot(Instances insts, String instsMark, String mark, String opponentMark, String targetClassValue) {
		this.checkForWinningMove = true;
		this.targetClassValue = targetClassValue;
		this.instsMark = instsMark;
		this.insts = insts;
		this.mark = mark;
		this.opponentMark = opponentMark;
		
		patterns = new ArrayList<Integer[]>() {{
			// Horizontal
			add(new Integer[] {0, 1, 2});
			add(new Integer[] {3, 4, 5});
			add(new Integer[] {6, 7, 8});

			// Vertical
			add(new Integer[] {0, 3, 6});
			add(new Integer[] {1, 4, 7});
			add(new Integer[] {2, 5, 8});

			// Diagonal
			add(new Integer[] {0, 4, 8});
			add(new Integer[] {2, 4, 6});
		}};
	}
	
	public abstract void init();

	public int getMove(String[] board) {
		if (checkForWinningMove) {
			int move = checkWinningMove(board, mark);
			if(move>=0){
				return move;
			}
	
			move = checkWinningMove(board, opponentMark);
			if(move>=0){
				return move;
			}
		}
		return makeDecision(board);
	}

	private int checkWinningMove(String[] board, String playerMark) {

		// Collect marks from pattern indexes
		String[][] patternMarks = new String[8][3];
		for (int i = 0; i < patterns.size(); i++) {
			Integer[] pattern = patterns.get(i);
			String[] marks = new String[pattern.length];
			for (int k = 0; k < marks.length; k++) {
				marks[k] = board[pattern[k]];
			}
			patternMarks[i] = marks;
		}

		// Check patterns of marks to have at most 1 blank and player marks otherwise
		PATTERN_MARKS_LOOP:
			for (int i = 0; i < patternMarks.length; i++) {
				int blankIndex = -1;
				String[] marks = patternMarks[i];
				for (int k = 0; k < marks.length; k++) {
					String mark = marks[k];
					if (mark.equals("b")) {
						if (blankIndex >= 0) {
							blankIndex = -1;
							continue PATTERN_MARKS_LOOP;
						} else {
							blankIndex = k;
						}
					} else if (mark.equals(playerMark)) {
						continue;
					} else {
						blankIndex = -1;
						continue PATTERN_MARKS_LOOP;
					}
				}
				if (blankIndex >= 0) {
					return patterns.get(i)[blankIndex];
				}
			}

			return -1;
	}

	protected abstract int makeDecision(String[] board);


	protected HashMap<Integer, Instance> generateNextInstances(String[] current, ArrayList<Attribute> attrs, Instances insts) {
		HashMap<Integer, Instance> nextMoves = new HashMap<Integer, Instance>();
		Instances nextInstances = new Instances("Next instances", attrs, 0);
		nextInstances.setClassIndex(insts.classIndex());
		double[] dummyArray = new double[attrs.size()];
		for (int i = 0; i < attrs.size() - 1; i++) {
			dummyArray[i] = attrs.get(i).indexOfValue(current[i]);
		}
		dummyArray[attrs.size() - 1] = attrs.get(attrs.size() - 1).indexOfValue("true");

		int playerMarkIndex = 0; // Initialize variable
		for (int i = 0; i < attrs.size()-1; i++) {

			if (current[i].equals("b") && i != insts.classIndex()) {
				// Get index of player mark in attribute values
				playerMarkIndex = attrs.get(i).indexOfValue(this.mark);

				// Create new instance of dummy array
				System.arraycopy(dummyArray, 0, dummyArray, 0, dummyArray.length);

				// Create new instance with playerMark on attribute i
				Instance nextInstance = new DenseInstance(0.0, dummyArray);
				nextInstance.setValue(i, playerMarkIndex);
				nextInstances.add(nextInstance);

				nextMoves.put(i, nextInstances.instance(nextInstances.numInstances() - 1));
			}
		}
		return nextMoves;
	}
	
	public void setWinningCheck(boolean state) {
		this.checkForWinningMove = state;
	}
	
	public boolean checksWinningMove() {
		return this.checkForWinningMove;
	}
	
	protected ArrayList<Attribute> getAttributes() {
		Attribute[] attrs = new Attribute[insts.numAttributes()];
		for (int i = 0; i < attrs.length; i++) {
			attrs[i] = insts.attribute(i);
		}
		return new ArrayList<>(Arrays.asList(attrs));
	}
	
	public void setSettings(String mark, String opponentMark, String targetClassValue) {
		this.mark = mark;
		this.opponentMark = opponentMark;
		this.targetClassValue = targetClassValue;
	}

}
