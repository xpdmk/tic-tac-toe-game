import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

import weka.classifiers.evaluation.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

public class J48Bot extends Bot {

	J48 tree;
	String[] options;

	public J48Bot(Instances insts, String instsMark, String mark, String opponentMark, String targetClassValue) {
		super(insts, instsMark, mark, opponentMark, targetClassValue);
		options = new String[] {"-M", "2", "-C", "0.5"};
	}
	
	@Override
	public void init() {
		this.tree = new J48();
		try {
			tree.setOptions(options);
			tree.buildClassifier(insts);
			
			Evaluation eval = new Evaluation(insts);
	    eval.crossValidateModel(tree, insts, 10, new Random(1));
	    System.out.println("Estimated Accuracy: "+Double.toString(eval.pctCorrect()));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
    
	}
	
	public void setOptions(String[] options) {
		this.options = options;
	}

	@Override
	protected int makeDecision(String[] board) {
		if (this.tree == null) {
			init();
		}

		ArrayList<Attribute> attrs = getAttributes(); // Attributes for all instances
		Map<Integer, Instance> nextMoves = generateNextInstances(board, attrs, insts); // [Empty spot index on board] pointing to [created instance when the spot is fill with bot's mark
		ArrayList<Instance> nextInsts = new ArrayList<>();
		nextInsts.addAll(nextMoves.values()); // Instance for each empty spot that has been filled with bot's mark
		double[][] confs = generateConfidences(nextInsts);

		int bestIndex = -1;
		double bestConf = -1.0;
		Attribute targetAttr = getAttributes().get(this.insts.classIndex());
		int indexTargetAttrValue = targetAttr.indexOfValue(this.targetClassValue); // Index of target attribute's value in target class attribute
		for (int i = 0; i < confs.length; i++) {
			double targetClassConf = confs[i][indexTargetAttrValue]; // Confidence for target value
			if (targetClassConf > bestConf) {
				bestIndex = i;
				bestConf = targetClassConf;
			}
		}
		
		ArrayList<Integer> moves = new ArrayList<>();
		moves.addAll(nextMoves.keySet()); // Extract indexes
		System.out.println(moves.get(bestIndex) + " = " + bestConf);
		
		return moves.get(bestIndex); // Return best index
	}
	
	private double[][] generateConfidences(ArrayList<Instance> insts) {
		int targetAttrValueCount = insts.get(0).attribute(0).numValues();
		int nextInstanceCount = insts.size();
		double[][] confidences = new double[nextInstanceCount][targetAttrValueCount];
		for (int instIndex = 0; instIndex < nextInstanceCount; instIndex++) {
			Instance inst = insts.get(instIndex);
			double[] dists = new double[2];
			try {
				dists = this.tree.distributionForInstance(inst);
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(0);
			}
			for (int distIndex = 0; distIndex < dists.length; distIndex++) {
				confidences[instIndex][distIndex] = dists[distIndex];
			}
		}
		return confidences;
	}
}
