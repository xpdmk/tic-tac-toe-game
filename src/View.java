import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class View extends Application {

	private Controller controller;
	private int columns = 3;
	private int rows = 3;
	private int gridSize = 100;
	private GridPane grid;
	private CheckBox winningCheckBoxBot;
	private Image playerMarksImage;
	private ComboBox<String> markMenu;
	private Rectangle2D x;
	private Rectangle2D o;
	private Stage primaryStage;

	@Override
	public void start(Stage primaryStage) {
		winningCheckBoxBot = new CheckBox("Check for winning moves");
		winningCheckBoxBot.setSelected(true);
		winningCheckBoxBot.selectedProperty().addListener(new ChangeListener<Boolean>() {
      public void changed(ObservableValue<? extends Boolean> ov,
          Boolean old_val, Boolean new_val) {
          controller.winningCheckStateChanged(new_val);
          System.out.println("Winning move checking set to " + new_val);
       }
     });
		
		ObservableList<String> options = 
		    FXCollections.observableArrayList(
		        "X",
		        "O"
		    );
		this.markMenu = new ComboBox<String>(options);
		this.markMenu.getSelectionModel().selectFirst();
		
		this.primaryStage = primaryStage;
		playerMarksImage = new Image("tic-tac-toe-hi.png");
		x = new Rectangle2D(310, 0, 271, 271);
		o = new Rectangle2D(0, 0, 271, 271);
		controller = new Controller(this);
		Scene start = getStartScene();
		primaryStage.setTitle("Tic Tac Toe");
		primaryStage.setScene(start);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
	
	public void setInteractive(Boolean isInteractive) {
		ObservableList<Node> panes = grid.getChildren();
		
		for (Node pane : panes) {
			if (isInteractive) {
				pane.setOnMouseReleased(e -> {
	      	int column = GridPane.getColumnIndex((Node)e.getSource());
	      	int row = GridPane.getRowIndex((Node)e.getSource());
	      	int index = row*3+column;
	      	if (controller.updateGame(index)) {
	      		controller.continueGame();
	      	} else {
	      		System.out.println("Don't cheat!");
	      	}
	      });
			} else {
				pane.setOnMouseReleased(e -> {});
			}
		}
	}
	
	public void showStart() {
		primaryStage.setScene(getStartScene());
	}
	
	public void showStart(String message) {
		Stage dialog = showDialog(message);
		dialog.setOnHidden(e -> {
			primaryStage.setScene(getStartScene());
		});
	}
	
	private Pane getStartLayout() {
		VBox layout = new VBox();
		Button startButton = new Button("Start");
		startButton.setOnMouseReleased(e -> {
			controller.setUserFirst(markMenu.getSelectionModel().selectedItemProperty().getValue() == "X");
			controller.newGame();
			primaryStage.setScene(ticTacToeScene());
			controller.continueGame();
		});
		layout.getChildren().addAll(winningCheckBoxBot, markMenu, startButton);
		return layout;
	}
	
	private Scene getStartScene() {
		return new Scene(getStartLayout());
	}

	public Scene ticTacToeScene() {		
		grid = new GridPane();
    grid.getStyleClass().add("game-grid");

    for(int i = 0; i < columns; i++) {
        ColumnConstraints column = new ColumnConstraints(gridSize);
        grid.getColumnConstraints().add(column);
    }

    for(int i = 0; i < rows; i++) {
        RowConstraints row = new RowConstraints(gridSize);
        grid.getRowConstraints().add(row);
    }

    for (int i = 0; i < columns; i++) {
        for (int j = 0; j < rows; j++) {
            Pane pane = new Pane();
            pane.getStyleClass().add("game-grid-cell");
            if (i == 0) {
                pane.getStyleClass().add("first-column");
            }
            if (j == 0) {
                pane.getStyleClass().add("first-row");
            }
            grid.add(pane, i, j);
        }
    }
    
    VBox layout = new VBox(winningCheckBoxBot, grid);
    Scene ticTacToe = new Scene(layout, (columns * gridSize) + 100, (rows * gridSize) + 100, Color.WHITE);
    ticTacToe.getStylesheets().add("game.css");
    return ticTacToe;
	}

	public void setMark(int index, String mark){
		int column = index%3;
		int row = (index-column)/3;
		int colIndex = column*3+row;
		Pane pane = (Pane) grid.getChildren().get(colIndex);
  	pane.getChildren().add(getMarkImage(mark));
	}

	private ImageView getMarkImage(String mark){
		Rectangle2D rect;
		if(mark.equals("o")){
			rect = o;
		} else {
			rect = x;
		}
		ImageView markImage = new ImageView(playerMarksImage);
		markImage.setViewport(rect);
		markImage.setFitWidth(gridSize);
    markImage.setPreserveRatio(true);
    markImage.setSmooth(true);
    markImage.setCache(true);
		return markImage;
	}

	private Stage showDialog(String message){
		final Stage dialog = new Stage();
    dialog.initModality(Modality.APPLICATION_MODAL);
    dialog.initOwner(primaryStage);
    VBox dialogVbox = new VBox(30);
    StackPane pane1 = new StackPane();
    pane1.getChildren().add(new Text(message));
    dialogVbox.getChildren().add(pane1);

    StackPane pane2 = new StackPane();
    Button btn = new Button();
    btn.setText("Ok");
    btn.setOnAction(event -> {
       Stage stage = (Stage) btn.getScene().getWindow();
       stage.close();
    });
    pane2.getChildren().add(btn);
    dialogVbox.getChildren().add(pane2);
    Scene dialogScene = new Scene(dialogVbox, 100, 100);
    dialog.setTitle("End");
    dialog.setScene(dialogScene);
    dialog.show();
    return dialog;
	}

	public void reset(){
		primaryStage.setScene(ticTacToeScene());
		primaryStage.show();
	}
}
