import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
 
import org.junit.Test;
import org.junit.Before;

import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import weka.core.Instances;
import weka.core.converters.CSVLoader;

import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(Parameterized.class)
public class J48BotXTest {
	static String X = "x";
	static String O = "o";
	static String B = "b";
	private String[] board;
	private int expectedResult;
	private J48Bot botX;
	
	@Before
	public void initialize() {
		CSVLoader loader = new CSVLoader();
		loader.setFieldSeparator(";");
		
		Instances instances = null;
		try {
			loader.setSource(new File("./tic-tac-toe.csv"));
			instances = loader.getDataSet();
		} catch (IOException e) {
			e.printStackTrace();
		}
		botX = new J48Bot(instances, "x", "x", "o", "true");
   }
	
   public J48BotXTest(String[] board, int expectedResult) {
      this.board = board;
      this.expectedResult = expectedResult;
   }

   @Parameterized.Parameters
   public static Collection getMove() {
      return Arrays.asList(new Object[][] {
         { new String[] { 
        		 X, X, B, 
        		 B, B, O, 
        		 B, B, O }, 2
         },
         { new String[] {
        		 X, B, B,
        		 X, O, O,
        		 O, X, X }, -1
         },
         { new String[] {
        		 O, X, X,
        		 B, B, B,
        		 B, B, O}, 4
         }
      });
   }

   @Test
   public void testGetMove() {
      int result = botX.getMove(board);
      System.out.println("Board: \n" + boardToString(board) + "\nResult: " + result + "\n");
      if (expectedResult < 0) {
      	assertNotEquals(expectedResult, result);
      } else {
      	assertEquals(expectedResult, result);
      }
   }
   
   public String boardToString(String[] board) {
  	 String string = "";
  	 for (int i = 0; i < board.length; i++) {
  		 string += board[i];
  		 if ((i + 1) % 3 == 0 && (i + 1) != board.length) 
  			 string += "\n";
  	 }
  	 return string;
   }
}
