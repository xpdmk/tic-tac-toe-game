import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

class ClassifierTest {
	String X = Classifier.X;
	String O = Classifier.O;
	String B = Classifier.B;
	Instances instances;
	ArrayList<Attribute> attrs;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		instances = Classifier.getCSVInstances("./tic-tac-toe.csv", ";");
		attrs = Classifier.getAttributes(instances.instance(0));
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testCheckWinningMove() {
		String targetAttrValue = "true";
		String playerMark = "x";
		String[] currentGameValues = new String[]
				{ 
						X, B, B,
						B, O, X,
						B, B, X,
						targetAttrValue
			  };
		Instance current = Classifier.createInstance(currentGameValues, attrs);
		int result = Classifier.checkWinningMove(current, playerMark);
		System.out.println(result);
		Assert.assertTrue(result == 2);		
	}

}
